"""
import requests
from bs4 import BeautifulSoup
import re
url = 'https://docs.python.org/3/'
r = requests.get(url)
soup = BeautifulSoup(r.text)
# Recherche par nom de balise
soup.p  # Equivalent a soup.find('p')
soup.find_all('p')

# Recherche par Regex

# Retourne les balises dont le nom ne contient que deux caractères
soup.find(re.compile(r'..'))

# Recherche par fonction
# Retourne les balises dont le nom ne contient que deux caractères


def f(tag):
    return len(tag.name) == 2


soup.find_all(f)

# Recherche par propriétés CSS
soup.select('.biglink')  # Recherche les éléments de la classe biglink

# Recherche par nom de balise + valeur d'un attribut
# Retourne les lien qui contiennent python dans l'url
soup.find('a', href=re.compile('python'))

"""
import re
import requests
from bs4 import BeautifulSoup
# Exercice 1
"""
UNI = requests.get("http://www.univ-orleans.fr")
print(UNI.text)

# Exercice 2

soup = BeautifulSoup(UNI.text, "lxml")
print(soup.h1)
print(soup.find_all('div'))
print(soup.find_all('img'))
print(soup.find_all('a'))
print(soup.find('div', class_='composite-zone'))

"""
# Exercice 3

"""
UNI = requests.get("https://stackoverflow.com/questions/tagged/beautifulsoup")
soup = BeautifulSoup(UNI.text, "lxml")

Questions = []
CountPost = []
Answer = []

for node in soup.findAll(class_='question-hyperlink', limit=10):
    Questions.append(''.join(node.findAll(text=True)))

for node in soup.findAll(class_='vote-count-post ', limit=10):
    CountPost.append(''.join(node.findAll(text=True)))

for node in soup.findAll(class_='status', limit=10):
    Answer.append(''.join(node.findAll(text=True))[1])

with open('reponse.csv', 'w') as file:
    file.write('Questions ; Votes ; Réponses ;')
    file.write('\n')
    for i in range(len(Answer)):
        file.write('"' + Questions[i] + '"; "' +
                   CountPost[i] + '"; "' + Answer[i] + '";')
        file.write('\n')
"""
# Exercice 4
"""
data = {"submit-form": "", "catalog": "catalogue-2015-2016", "degree": "DP"}
r = requests.post(
    "http://formation.univ-orleans.fr/fr/formation/rechercher-une-formation.html#nav", data=data)
formations = []
soup = BeautifulSoup(r.text, "lxml")
for node in soup.find_all('li', class_='hit'):
    for formation in node.findAll('strong'):
        formations.append(''.join(formation.findAll(text=True)))
print(formations)
"""
# Exercice 5

def get_definition(x):
    URL ='http://services.aonaware.com/DictService/Default.aspx?action=define&dict=wn&query={0}'.format(x)
    html = requests.get(URL).text
    soup = BeautifulSoup(html, "lxml")
    definitions=[]
    for node in soup.find_all('pre',attrs={"class":None}):
        definitions.append(''.join(node.findAll(text=True)))
    return definitions

lines=[]
with open('vocabulary.txt') as f:
    lines=f.readlines() 

with open('definitions.txt','w') as d:
    for mot in lines:
        d.write(get_definition(mot)[0])